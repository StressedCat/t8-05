#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from Libro import book


if __name__ == '__main__':
    x = 0
    while x != 10:
        confirm = False
        '''
        Se mostrara la categoria de libros que hay hasta que el usuario ingrese
        X, terminando el programa que se realizaba, en caso de que ingrese un
        valor que no daba resultado, se dira explicitamente que lo intente de
        nuevo
        '''
        print("\n")
        print("Eliga el libro que quiere ver: ")
        print("para un libro emocionante, presione E")
        print("Para un libro de terror, presione T")
        print("Para un libro imposible de leer, presione A")
        print("Para un libro de la naturaleza, presione N")
        print("Presione X para salir")
        print("\n")
        opt = input("Haga su opcion: ")
        if opt.upper() == "E":
            Book = book("Top 10 Animes", "Marco Polo", 30, 13)
            confirm = True
        elif opt.upper() == "T":
            Book = book("7 historias de terror", "Dross", 7, 2)
            confirm = True
        elif opt.upper() == "A":
            Book = book("como dibujar 101", "Ale", 100, 31)
            confirm = True
        elif opt.upper() == "N":
            Book = book("El libro de la selva", "La selva", 38, 12)
            confirm = True
        elif opt.upper() == "X":
            print("\n")
            print("Gracias, vuelva pronto con nuestra gran variedad de libros")
            quit()
        else:
            print("Su eleccion es incorrecta, intentelo de nuevo")
        if confirm is True:
            print("El libro es {0} del autor {1}".format(Book.Titulo,
                                                            Book.Autor))
            print("Se tienen {0} copias y se prestaron {1}".format(Book.Total,
                                                                    Book.No))
