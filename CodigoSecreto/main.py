#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import random
from Codigo import numero


if __name__ == '__main__':
    '''
        Se conseguira el codigo por medio de valores al azar del 0 al 9, para
        comprobar de que funcionara con cualquier numero que se le de
        La u viene de unidad, d de decena, c de centena y m de mil
    '''
    u = random.randint(0, 9)
    d = random.randint(0, 9)
    c = random.randint(0, 9)
    m = random.randint(0, 9)
    Ini = numero(m, c, d, u)
    print("El numero inicial es: {0}{1}{2}{3}".format(Ini.mil,
                                                        Ini.centena,
                                                        Ini.decena,
                                                        Ini.unidad))
    un = (u + d + c + m) % 10
    dn = (d + c + m) % 10
    cn = (c + m) % 10
    mn = m
    u7 = (un + 7) % 10
    d7 = (dn + 7) % 10
    c7 = (cn + 7) % 10
    m7 = (mn + 7) % 10
    uf = c7
    df = m7
    cf = u7
    mf = d7
    Final = numero(mf, cf, df, uf)
    print("El numero codificado es: {0}{1}{2}{3}".format(Final.mil,
                                                            Final.centena,
                                                            Final.decena,
                                                            Final.unidad))
