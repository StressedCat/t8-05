#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from Tarjeta import Card
from Actualizar import cambio


def Retirar(Saldo):
    Ret = int(input("¿Cuanto desea retirar?"))
    total = cambio(Saldo - Ret)
    Info1.ActualizarSaldo(total)


def Depositar(Saldo):
    Dep = int(input("¿Cuanto desea depositar? \n"))
    total = cambio(Saldo + Dep)
    Info1.ActualizarSaldo(total)


if __name__ == '__main__':
    '''
        Se creara una tarjeta de "Ale" la cual tendra 65000 pesos
    '''
    Choice = None
    Info1 = Card("4881-9487-2791-1041", "Ale", 65000)
    '''
        Se hara la eleccion del usuario para depositar o retirar de su tarjeta
        siendo actualizada por medio de "Actualizar.py"
    '''
    while Choice != "X":
        print("La tarjeta {0} de {1} tiene ${2}".format(Info1.numero,
                                                        Info1.nombre,
                                                        Info1.saldo))
        print("\n")
        print("¿Desea realizar alguna operacion?")
        print("Presione d para depositar")
        print("Presione r para retirar")
        print("presione x para terminar su operacion")
        Choice = input("Ingrese su opcion: ")
        print("\n")
        if Choice.upper() == "D":
            Depositar(Info1.saldo)
        elif Choice.upper() == "R":
            Retirar(Info1.saldo)
        elif Choice.upper() == "X":
            print("Muchas gracias por usar la aplicacion")
            quit()
            pass
        else:
            print("Tecla erronea, intente de nuevo")
