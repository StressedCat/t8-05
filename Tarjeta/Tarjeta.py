#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from Actualizar import cambio


class Card():
    '''
        Aca se guarda los datos de la tarjeta, en el caso que el saldo es
        cambiado ira hacia Actualizar saldo, donde se cambiara al saldo actual
        que se tiene ahora
    '''
    def __init__(self, numero, nombre, saldo):
        self.numero = numero
        self.nombre = nombre
        self.saldo = saldo

    def ActualizarSaldo(self, SaldoCambio):
        self.saldo = SaldoCambio.actualizar
