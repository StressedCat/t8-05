#!/usr/bin/env python3
# -*- coding: utf-8 -*-


class Perro():
    def __init__(self, color, raza, nombre, sexo, peso, edad):
        self.color = color
        self.raza = raza
        self.nombre = nombre
        self.sexo = sexo
        self.peso = peso
        self.edad = edad
