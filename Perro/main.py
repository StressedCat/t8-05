#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from Perrito import Perro


def Calificar():
    print("\n")
    print("Su mascota de nombre {} se ve increible".format(Firu.nombre))
    print("Su pelaje {} resalta demasiado, es perfecto".format(Firu.color))
    print("Su raza {} es algo inusual, pero positivamente".format(Firu.raza))
    print("Teniendo {} años y pesando {} kg tiene un balance".format(Firu.edad,
                                                                    Firu.peso))
    if S.upper() == 'F':
        print("Su serenidad da una tranquilidad en el cuarto")
        print("Irradiando harmonia con quien sea que este")
        print("Talvez parezca algo inmovil, ¡pero es lo suficiente!")
        pass
    elif S.upper() == 'M':
        print("Su espiritu siempre activo y corriendo por todos lados")
        print("Es capaz de hacer lo que sea para proteger su familia")
        print("Algo ruidoso, ladrando a todo, ¡pero es soportable!")
    print("Su mascota es muy increible, siga dandole sus cuidados")
    print("¡Cada mascota tiene algo especial! quierelas siempre como familia")
    pass

if __name__ == '__main__':
    Choice = False
    '''
        Todo comienza con un texto explicando de lo que se hará, junto con esto
        se hara un cuestionario de su mascota para realizar la clasificacion.
        Todas los valores ingresados seran usado en la clasificacion, mientras
        que el de genero o sexo de la mascota cambiara ligeramente su actitud
    '''
    print("Bienvenido a la historia interactiva: 'Calificador de perros'")
    print("Por favor, cree su perro que sera protagonista de esta historia")
    N = input("\n¿Que nombre tendra su perro?\n>")
    R = input("\n¿Que raza será?\n>")
    C = input("\n¿Que color es su pelaje?\n>")
    P = input("\n¿Cuanto pesará en kg?\n>")
    E = input("\n¿Que edad tiene?\n>")
    print("\n¿Que sexo es?")
    while Choice is False:
        S = input("Presione F si es femenino\nPresione M si es masculino\n>")
        if S.upper() == 'F':
            Sex = "Femenino"
            Choice = True
        elif S.upper() == 'M':
            Sex = "Masculino"
            Choice = True
        else:
            print("Dato erroneo, intente de nuevo")
    '''
        Al terminar el cuestionario se entrara a una def la cual contara la
        historia con el grupo "Firu" el cual es el de nuestro perro definido
        anteriormente
    '''
    Firu = Perro(C, R, N, Sex, P, E)
    Calificar()
