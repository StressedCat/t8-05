#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import random
from Cuenta import CuentaBancaria
from Transferencias import Cambios


def Retirar(Fondos):
    Comprobar = False
    Ret = float(input("¿Cuanto desea retirar?\n"))
    total = Cambios(Fondos - Ret)
    if Fondos - Ret >= 0:
        Comprobar = True
    if Comprobar is True:
        In1.ActualizarFondos(total)
    elif Comprobar is not True:
        print("\nSus fondos no son suficientes\n")


def Depositar(Fondos):
    Dep = float(input("¿Cuanto desea depositar?\n"))
    total = Cambios(Fondos + Dep)
    In1.ActualizarFondos(total)


if __name__ == '__main__':
    '''
        se crea la informacion de la persona, preguntando su nombre y con un
        random se obtiene los fondos en su cuenta, las opciones que se
        presentan son depositar y retirar, si retirar supera los fondos que
        se tienen, este sera rechazado con el mensaje de que no tienes los
        fondos
    '''
    Choice = None
    Ran = random.randint(0, 9)
    fondos = float(10000 * Ran)
    nom = input("Buenos dias usuario, ¿cual es su nombre?\n")
    In1 = CuentaBancaria("2387-6269-3458-4358", nom, fondos)
    while Choice != "X":
        print("\nTarjeta: {0}\nUsuario: {1}\nFondos: ${2}".format(In1.Num,
                                                                    In1.Nom,
                                                                    In1.Fon))
        print("\n")
        print("¿Desea realizar alguna operacion?")
        print("Presione d para depositar")
        print("Presione r para retirar")
        print("presione x para terminar su operacion")
        Choice = input("Ingrese su opcion: ")
        print("\n")
        if Choice.upper() == "D":
            Depositar(In1.Fon)
        elif Choice.upper() == "R":
            Retirar(In1.Fon)
        elif Choice.upper() == "X":
            print("Muchas gracias por usar la aplicacion")
            quit()
            pass
        else:
            print("Tecla erronea, intente de nuevo")
