#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from Transferencias import Cambios


class CuentaBancaria():
    def __init__(self, Num, Nom, Fon):
        self.Num = Num
        self.Nom = Nom
        self.Fon = Fon

    def ActualizarFondos(self, Cambio):
        self.Fon = Cambio.SaldoNuevo
